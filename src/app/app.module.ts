import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { HomeModule } from './pages/home/home.module';
import { TitleBarComponent } from './title-bar/title-bar.component';
import { MainMenuService } from './main-menu/main-menu.service';

@NgModule({
  declarations: [
    AppComponent,
    MainMenuComponent,
    TitleBarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    HomeModule
  ],
  providers: [MainMenuService],
  bootstrap: [AppComponent]
})
export class AppModule { }
