import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FriendlyBoxComponent } from './friendly-box.component';

describe('FriendlyBoxComponent', () => {
  let component: FriendlyBoxComponent;
  let fixture: ComponentFixture<FriendlyBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FriendlyBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FriendlyBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
