import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FriendlyBoxComponent } from './friendly-box/friendly-box.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [FriendlyBoxComponent],
  exports: [
    FriendlyBoxComponent
  ]
})
export class SharedModule { }
