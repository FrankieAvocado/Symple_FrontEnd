import { Component, OnInit } from '@angular/core';
import { MainMenuData } from './main-menu.data';
import { NavLinkModel } from '../../models/nav-link.model'
import { MainMenuService } from './main-menu.service';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit {

  public links : Array<NavLinkModel>;
  constructor(public mainMenuService : MainMenuService) { }

  ngOnInit() {
    this.links = MainMenuData;
  }

}
