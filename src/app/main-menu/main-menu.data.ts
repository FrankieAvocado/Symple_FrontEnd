import { NavLinkModel } from '../../models/nav-link.model'

export const MainMenuData : Array<NavLinkModel> = [
  {
    url: 'http://www.google.com',
    text: 'Class Information',
    icon: 'fa-calendar-check-o'
  },
  {
    url: 'http://www.google.com',
    text: 'User Information',
    icon: 'fa-user-circle-o'
  },
  {
    url: 'http://www.google.com',
    text: 'Account',
    icon: 'fa-id-card'
  },
  {
    url: 'http://www.google.com',
    text: 'Settings',
    icon: 'fa-cogs'
  }
];
