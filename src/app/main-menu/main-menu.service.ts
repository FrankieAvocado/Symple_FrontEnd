import { Injectable } from '@angular/core';

@Injectable()
export class MainMenuService {

  private expanded: boolean = false;
  constructor() { }

  public toggleMenu = () => {
    this.expanded = !this.expanded;
  }

  public get isMenuExpanded():boolean{
    return this.expanded;
  }

}
