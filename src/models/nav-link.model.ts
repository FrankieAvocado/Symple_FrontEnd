export class NavLinkModel {
  url: string;
  text: string;
  icon: string;
};
